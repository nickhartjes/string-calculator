package nl.nickhartjes.stringcalculator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringReturnsZero() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(0, stringCalculator.add(""));
    }

    @Test
    public void oneStringReturnsOne() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(1, stringCalculator.add("1"));
    }

    @Test
    public void oneAndTwoStringReturnsTwelve() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(12, stringCalculator.add("12"));
    }

    @Test
    public void oneAndTwoStringWithCommaReturnsThree() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(3, stringCalculator.add("1,2"));
    }

    @Test
    public void oneAndTwoStringWithLineBreakReturnsThree() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(3, stringCalculator.add("1\n2"));
    }

    @Test
    public void oneTwoAndThreeStringWithLineBreakAndCommaReturnsSix() {
        StringCalculator stringCalculator = new
                StringCalculator();
        assertEquals(6, stringCalculator.add("1\n2,3"));
    }

    @Test
    public void incorrectInputTest() {
        StringCalculator stringCalculator = new StringCalculator();
        assertEquals(1, stringCalculator.add("1,\n"));
    }
}

