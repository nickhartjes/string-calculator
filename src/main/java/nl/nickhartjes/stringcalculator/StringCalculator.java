package nl.nickhartjes.stringcalculator;

public class StringCalculator {

    private String[] delimiters = {",", "\n"};

    private String createRegEx() {
        StringBuilder regExBuilder = new StringBuilder();
        regExBuilder.append("[");
        for (String delimiter : delimiters) {
            regExBuilder.append("/");
            regExBuilder.append(delimiter);
        }
        regExBuilder.append("]");
        return regExBuilder.toString();
    }

    public int add(String numberString) {
        int sum = 0;
        String regEx = this.createRegEx();
        String[] numberStringArray = numberString.split(regEx);

        for (String item : numberStringArray) {
            try {
                int parsedInt=  Integer.parseInt(item);
                if (parsedInt < 1){
                    throw new IllegalArgumentException("A value between the delimiter is empty");
                }
                sum += parsedInt;
            } catch (NumberFormatException e){
                throw new IllegalArgumentException("Value is can't be parsed to integer");
            }
        }
        return sum;
    }
}

